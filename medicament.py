class Medicament:

    nom = ""
    prix = 0
    stock = 0

    def __init__(self, nom, prix, stock):
        self.nom = nom
        self.prix = prix
        self.stock = stock

    def approvisionnement(self, quantite_aprovisione):
        self.stock += quantite_aprovisione

    def debit(self, quantite_debite):
        self.stock = self.stock - quantite_debite

    def __str__(self):
        return ("Medicament(name={}, prix={}, stock={})".format(self.nom, self.prix,self.stock))