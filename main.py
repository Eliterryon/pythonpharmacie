from medicament import Medicament
from client import Client
import unicodedata



liste_client = []
liste_medicament = []



def affichage():
    print("Affichage des stocks")
    for medoc in liste_medicament :
        print(medoc)
    print("Affichage des credits")
    for client in liste_client :
        print(client)

def approvisionnement():
    medoc = None
    print("Nom du Medicament?:")
    while medoc is None :
        medoc = lire_table(liste_medicament)
        if medoc is None :
            print("Medicament inconnu. Veuilliez recommencer")

    print("Donner la Quantite :")
    quantite = typed_input()
    medoc.approvisionnement(quantite)

def achat():
    medoc = None
    client = None
    print("Nom du Medicament?:")
    while medoc is None :
        medoc = lire_table(liste_medicament)
        if medoc is None :
            print("Medicament inconnu. Veuilliez recommencer")

    print("Nom du client?:")
    while client is None :
        client = lire_table(liste_client)
        if client is None :
            print("Client inconnu. Veuilliez recommencer")

    print("quel est le montant du paiement?")
    quantite_paiyment = typed_input(float)
    print("quelle est la quantite achetee?")
    quantite_medicament = typed_input()

    while( quantite_medicament > medoc.stock ):
        print("le stock pour ce medicament est de :" + str(medoc.stock) + " veuilleur choisire un nombre valable")
        quantite_medicament = typed_input()

    medoc.debit(quantite_medicament)
    prix = quantite_paiyment - ( quantite_medicament * medoc.prix )
    client.crediter(prix)


def quitter():
    print ("Programme termine!")

 ##################### lecture des tables ########################

def lire_table(Liste):
    nom = input()
    for obj in Liste :
        if (strip_accents_and_lower(obj.nom) == strip_accents_and_lower(nom)):
            return obj
    return None

 ##################### definition des inpute #####################

def typed_input(type = int):
    while True :
        floatInpute = input()
        try:
            valeur = type(floatInpute)
            return valeur
        except:
            print("Veuillez entrer un " + type.__name__)

##################### outil divers ###############################

def strip_accents_and_lower(s):
   return ''.join(c for c in unicodedata.normalize('NFD', s)
                  if unicodedata.category(c) != 'Mn').lower()

##################### main loop ##################################

liste_client.append(Client("Malfichu",0.0))
liste_client.append(Client("Palichon",0.0))
liste_medicament.append(Medicament("Aspiron", 20.40, 5))
liste_medicament.append(Medicament("Rhinoplexil", 19.15, 5))

numberIn = ""
while numberIn != "4":
    print("")
    print("")
    print("1 : Achat de medicament")
    print("2 : Approvisionnement en  medicaments")
    print("3 : Etats des stocks et des credits")
    print("4 : Quitter")
    numberIn = input()
    if (numberIn == "1" ):
        achat()
    elif (numberIn == "2" ):
        approvisionnement()
    elif (numberIn == "3" ):
        affichage()
    elif (numberIn == "4" ):
        quitter()
        break
